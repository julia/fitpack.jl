module FitPack

import Libdl

# create libcdi symbole
@static if Sys.iswindows()
    @error "Windows is currently not supported"
    const libfitpack = ""
elseif Sys.isapple() || Sys.islinux()
    const BASEPATH = @__DIR__
    const libfitpack = Libdl.find_library(["libfitpack", "fitpack"], [joinpath(BASEPATH, "..", "fitpack")])
else
    @warn "Your platform couldn't detected. Use the fallback"
    const libfitpack = Libdl.find_library(["libfitpack", "fitpack"], [joinpath(BASEPATH, "..", "fitpack")])
end

const sigma = 1.0
const islpsw = 3    
const slp1 = 0.0
const slpn = 0.0    

ft_configuration = Dict("sigma" => 1.0, "islpsw" => 3, "slp1" => 0.0, "slpn" => 0.0) 

function ftsetp(name::String, value)    
    ft_configuration[name] = value
    sigma = ft_configuration["sigma"]
    islpsw = ft_configuration["islpsw"]
    slp1 = ft_configuration["slp1"]
    slpn = ft_configuration["slpn"]
    return
end
export ftsetp

function ftgetp(name::String)
    return ft_configuration[name]
end
export ftgetp

function ftcurv(x0::Vector{Float64}, y0::Vector{Float64}, x::Vector{Float64})::Vector{Float64}

    n::Int32 = size(x0, 1)
    np2 = n + 2

    yp = similar(y0)
    temp = zeros(Float64, np2)

    ierr::Int32 = 0
    ccall((:curv1_, libfitpack),
          Cvoid,
          (Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}, Ref{Float64}, Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}, Ref{Int32}), 
          n, x0, y0, slp1, slpn, islpsw, yp, temp, sigma, ierr)

    y = similar(x)
    for i in 1:size(x, 1)
        y[i] = ccall((:curv2_, libfitpack),
                     Float64,
                     (Ref{Float64}, Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}),
                     x[i], n, x0, y0, yp, sigma)::Float64
    end

    return y    
end
export ftcurv

function ftcurvd(x0::Vector{Float64}, y0::Vector{Float64}, x::Vector{Float64})::Vector{Float64}

    n::Int32 = size(x0, 1)
    np2 = n + 2

    yp = similar(y0)
    temp = zeros(Float64, np2)

    ierr::Int32 = 0
    ccall((:curv1_, libfitpack),
          Cvoid,
          (Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}, Ref{Float64}, Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}, Ref{Int32}), 
          n, x0, y0, slp1, slpn, islpsw, yp, temp, sigma, ierr)

    y = similar(x)
    for i in 1:size(x, 1)
        y[i] = ccall((:curvd_, libfitpack),
                     Float64,
                     (Ref{Float64}, Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}),
                     x[i], n, x0, y0, yp, sigma)::Float64
    end
    
    return y
end
export ftcurvd

"""
# Parameter
- `xl`: lower bounds of the integral
- `xu`: uper bounds of the integral
- `x0`: x values of the incoming function
- `y0`: y values of the incoming function

# Return
An array of size of xl with the results

# Assertion
- `size(xl) == size(xu)`
"""
function ftcurvi(xl::Vector{Float64}, xu::Vector{Float64}, x0::Vector{Float64}, y0::Vector{Float64})::Vector{Float64}
    @assert size(xl) == size(xu) "xl and xu dose not have the same size"

    n::Int32 = size(x0, 1)
    np2 = n + 2
    
    yp = similar(y0)
    temp = zeros(Float64, np2)
    
    ierr::Int32 = 0    
    ccall((:curv1_, libfitpack),
          Cvoid,
          (Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}, Ref{Float64}, Ref{Int32}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}, Ref{Int32}), 
          n, x0, y0, slp1, slpn, islpsw, yp, temp, sigma, ierr)

    y = similar(xu)
    for i in 1:size(xu, 1)
        y[i] = ccall((:curvi_, libfitpack),
                     Float64,
                     (Ref{Float64}, Ref{Float64}, Ref{Int32},  Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ref{Float64}),
                     xl[i], xu[i], n, x0, y0, yp, sigma)::Float64
    end

    return y
end
ftcurvi(xl::Float64, xu::Float64, x0::Vector{Float64}, y0::Vector{Float64}) = ftcurvi([xl], [xu], x0, y0)[1]

export ftcurvi

end
