#! /usr/bin/env julia

using DataFrames
using CSV
using FitPack

const path = joinpath(@__DIR__,"raw_data.csv") |> normpath
test_data = CSV.read(path, DataFrame, header=1, delim=',')

ftsetp("sigma", 1.0)
@info "sigma: " * string(ftgetp("sigma"))
@info "islpsw: " * string(ftgetp("islpsw"))
@info "slp1: " * string(ftgetp("slp1"))
@info "slpn: " * string(ftgetp("slpn"))

x = test_data[:,1]
y = test_data[:,2]

d = ftcurvd(x, y, x)
v = ftcurv(x, y, x)
i1 = ftcurvi([-1.0, -2.0], [1.0, 2.0], x, y)
i2 = ftcurvi(-1.0, 1.0, x, y)
