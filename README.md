FitPack.jl
==========

*Yet another Julia library for 1-d splines under tension*

This is a Julia wrapper for the [fitpack](https://www.netlib.org/fitpack/) Fortran library,
the same library underlying the ft* interpolation functions in [ncl](https://www.ncl.ucar.edu/).

### Features

Supported are the Float64 versions only, as are:

- ftcurv
- ftcurvd
- ftcurvi
- ftsetp
- ftgetp

Function arguments are the same as given in the manual of [ncl](https://www.ncl.ucar.edu/).

### Install (Julia 1.5.2 and maybe later)


```julia
(v1.5) pkg> add FitPack
```

The Fortran library source code is distributed with the package, so
you need a Fortran compiler. 

### Example usage


see `example/test-fitpack.jl`
