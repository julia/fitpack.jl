const BASEPATH = @__DIR__

@info "Building fitpack Fortran77 shared library of fitpack ..."
cd(joinpath(BASEPATH, "..", "fitpack"))
@info "   clean-up first"
run(`make clean`)
@info "   build"
run(`make`)
